# Searx

[Searx](https://github.com/asciimoo/searx/) A privacy-respecting, hackable metasearch engine.

## Access

It is available at [https://searx.{{ domain }}/](https://searx.{{ domain }}/) or [http://searx.{{ domain }}/](http://searx.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://searx.{{ tor_domain }}/](http://searx.{{ tor_domain }}/)
{% endif %}
